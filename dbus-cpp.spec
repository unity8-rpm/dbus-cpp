
%define sover 5

Name:		dbus-cpp
Version:	5.0.0+16.10.20160809
Release:	1
Summary:	A header-only dbus-binding leveraging C++-11
License:	LGPL
Group:		Development/Libraries
Url:		https://launchpad.net/dbus-cpp
Source0:	https://launchpad.net/ubuntu/+archive/primary/+files/%{name}_%{version}.orig.tar.gz
Patch0:		dbus-cpp-no-gtest.patch
Patch1:		boost-asio-1-66.patch
BuildRequires:	gcc-c++
BuildRequires:	cmake
BuildRequires:	boost-devel >= 1.58
%if %{defined suse_version} && 0%{?suse_version} > 1330
BuildRequires:	libboost_filesystem-devel >= 1.58
BuildRequires:	libboost_program_options-devel >= 1.58
BuildRequires:	libboost_system-devel >= 1.58
%endif
BuildRequires:	pkgconfig(libxml-2.0)
BuildRequires:	pkgconfig(dbus-1)
BuildRequires:	pkgconfig(process-cpp)
%if %{defined suse_version}
BuildRequires:	lsb-release
BuildRequires:	distribution-release
%endif
%if %{defined fedora}
BuildRequires:	redhat-lsb-core
%endif
BuildRoot:	%{_tmppath}/%{name}-%{version}-build

%description
A header-only dbus-binding leveraging C++-11.

%if %{defined suse_version}
%debug_package
%endif


%package -n libdbus-cpp%{sover}
Summary:	 C++11 library for handling processes - runtime library
Group:		System/Libraries

%description -n libdbus-cpp%{sover}
A header-only dbus-binding leveraging C++-11.

This package provides shared libraries for %{name}.


%package devel
Summary:	%{summary}
Group:		Development/Libraries
Requires:	libdbus-cpp%{sover} = %{version}

%description devel
A header-only dbus-binding leveraging C++-11.

This package provides development headers for %{name}.


%prep
%setup -q -c
%patch0 -p1
%patch1 -p1

%build
mkdir build
pushd build
cmake .. \
	-DCMAKE_C_FLAGS="%{optflags}" \
	-DCMAKE_CXX_FLAGS="%{optflags} -std=c++11" \
	-DCMAKE_INSTALL_LIBDIR=%{_libdir} \
	-DCMAKE_INSTALL_PREFIX=%{_prefix} \
	-DCMAKE_BUILD_TYPE=RelWithDebInfo
make %{?_smp_mflags}
popd

%install
pushd build
%make_install
rm -r %{buildroot}/%{_prefix}/libexec/examples
popd


%files
%defattr(-,root,root)
%doc COPYING.GPL COPYING.LGPL
%{_bindir}/dbus-cppc


%files -n libdbus-cpp%{sover}
%defattr(-,root,root)
%{_libdir}/libdbus-cpp.so.%{sover}*


%files devel
%defattr(-,root,root)
%{_libdir}/libdbus-cpp.so
%dir %{_includedir}/core
%dir %{_includedir}/core/dbus
%{_includedir}/core/dbus/*
%{_libdir}/pkgconfig/dbus-cpp.pc
%{_datadir}/dbus-cpp


%post -n libdbus-cpp%{sover}
/sbin/ldconfig

%postun -n libdbus-cpp%{sover}
/sbin/ldconfig


%changelog

